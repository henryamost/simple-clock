package com.example.jian_dan_clock

import android.graphics.Typeface

import android.annotation.SuppressLint
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import java.util.Locale

import android.os.Handler
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.widget.TextView
import androidx.annotation.RequiresApi
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var textView: TextView
    private val handler = Handler()

    @RequiresApi(Build.VERSION_CODES.N)
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // 获取 ClockView 和 TextView 的引用
        textView = findViewById(R.id.clock_view)

        // 设置一个初始时间
        updateCurrentTime()

        // 创建一个Runnable，用于实时更新当前时间
        val updateTimeRunnable = object : Runnable {
            @RequiresApi(Build.VERSION_CODES.N)
            override fun run() {
                updateCurrentTime()
                // 每隔一秒更新一次时间
                handler.postDelayed(this, 1000)
            }
        }

        // 开始实时更新时间
        handler.post(updateTimeRunnable)
        textView.setTypeface(null, Typeface.BOLD)
    }

    // 更新当前时间
    @RequiresApi(Build.VERSION_CODES.N)
    private fun updateCurrentTime() {
//        val currentTime = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())
//        val currentDate = SimpleDateFormat("MM月dd日 E", Locale.getDefault()).format(Date())
//        textView.text = "中国标准时间\r\n$currentDate"
        val currentTime = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())
        val currentDate = SimpleDateFormat("MM月dd日 EEEE", Locale.getDefault()).format(Date())
        var forntColor = Color.rgb(24, 25, 27)
        var forntColorS = Color.rgb(133, 134, 138)
        val redColor = forntColor
        val grayColor = forntColorS
//        val boldTypeface = Typeface.defaultFromStyle(Typeface.BOLD)

        // 创建一个 SpannableString 对象，用于设置不同样式和颜色的文本
        val spannableString = SpannableString("中国标准时间\n  $currentDate")

        // 设置“中国标准时间”文本的样式和颜色
        spannableString.setSpan(
            ForegroundColorSpan(redColor), // 设置文本颜色为红色
            0, 6, // 设置文本的起始位置和结束位置
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE // 设置标志位
        )
        spannableString.setSpan(
            StyleSpan(Typeface.BOLD), // 设置文本样式为加粗
            0, 6,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        // 设置日期的样式和颜色
        spannableString.setSpan(
            RelativeSizeSpan(0.7f), // 设置文本大小为原始大小的0.8倍，即缩小一号
            7, spannableString.length, // 日期部分的起始位置和结束位置
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        spannableString.setSpan(
            ForegroundColorSpan(grayColor), // 设置文本颜色为灰色
            7, spannableString.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )


        textView.text = spannableString
    }

    override fun onDestroy() {
        super.onDestroy()
        // 移除更新时间的Runnable，避免内存泄漏
        handler.removeCallbacksAndMessages(null)
    }
}