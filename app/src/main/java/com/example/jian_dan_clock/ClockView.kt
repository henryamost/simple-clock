package com.example.jiandan_clocks

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import java.util.*

class ClockView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    // Draw background circle
    val lightGray = Color.rgb(247, 248, 252) // 使用 RGB 值创建浅灰色
    val lightGrayPoint = Color.rgb(190, 198, 216) // 使用 RGB 值创建浅灰色
    val minLine = Color.rgb( 78, 79, 83) // 使用 RGB 值创建浅灰色
    val cycleLine = Color.rgb( 247,248,252) // 使用 RGB 值创建浅灰色
    val backgroundPaint = Paint().apply {
        color = lightGray // 浅灰色
        style = Paint.Style.FILL
    }
    private val circlePaint = Paint().apply {
        color = cycleLine
        strokeWidth = 25f
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.ROUND
        isAntiAlias = true
    }

    private val numberPaint = Paint().apply {
        color = Color.BLACK
        textSize = 70f
        textAlign = Paint.Align.CENTER
        isAntiAlias = true
        isFakeBoldText = true // 设置文本加粗
    }

    private val linePaint = Paint().apply {
        color = lightGrayPoint
        strokeWidth = 5f
        isAntiAlias = true
    }

    val centerCirclePaint = Paint().apply {
        color = Color.RED // 圆圈点的颜色，这里假设为红色
        style = Paint.Style.FILL
    }
    private val hourHandPaint = Paint().apply {
        color = Color.BLACK  // 时针的颜色为黑色
        style = Paint.Style.STROKE  // 绘制样式为描边，即只绘制线条不填充内部
        strokeWidth = 12f  // 线条宽度为4像素
        isAntiAlias = true  // 开启抗锯齿，使线条更加平滑
    }

    // 初始化分针绘制样式
    private val minuteHandPaint = Paint().apply {
        color = minLine  // 分针颜色
        style = Paint.Style.STROKE  // 描边样式
        strokeWidth = 8f  // 线条宽度
        strokeCap = Paint.Cap.ROUND  // 线条端点样式为圆角
        isAntiAlias = true  // 抗锯齿
    }

    // 初始化秒针绘制样式
    private val secondHandPaint = Paint().apply {
        color = Color.RED  // 秒针颜色，通常使用醒目的颜色
        style = Paint.Style.STROKE  // 描边样式
        strokeWidth = 4f  // 线条宽度，通常比分钟线细
        strokeCap = Paint.Cap.ROUND  // 线条端点样式为圆角
        isAntiAlias = true  // 抗锯齿
    }

    private var centerX = 0f
    private var centerY = 0f
    private var radius = 0f

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        centerX = w / 2f
        centerY = h / 2f
        radius = (w.coerceAtMost(h) / 2f) - 10f
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        drawClockFace(canvas)
        drawClockHandss(canvas)
        drawHourNumbers(canvas) // 添加小时数字
        drawMinuteLines(canvas) // 绘制分钟刻度
//        drawClockText(canvas)

        // 每隔一秒钟重绘一次
        postInvalidateDelayed(1000)
    }


    private fun drawClockFace(canvas: Canvas) {
        canvas.drawCircle(centerX, centerY, radius, backgroundPaint)
        canvas.drawCircle(centerX, centerY, radius, circlePaint)

        // 绘制钟表中心点
//        canvas.drawPoint(centerX, centerY, circlePaint)
        canvas.drawCircle(centerX, centerY, radius * 0.025f, centerCirclePaint) // 调整圆圈点的大小和颜色

    }


    /**
     * 在画布上绘制时钟的时针、分针和秒针。
     *
     * @param canvas 画布对象，用于绘制时钟指针。
     */
    private fun drawClockHandss(canvas: Canvas) {
        // 获取当前时间
        val calendar = Calendar.getInstance()
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)
        val second = calendar.get(Calendar.SECOND)
        println("[$hour] [$minute] $second")

        // 根据当前时间计算各个指针的角度
        val hourAngle = (hour * 30f + minute * 0.5f) % 360
        val minuteAngle = (minute * 6f) % 360
        val secondAngle = (second * 6f) % 360
        // 绘制时针
        canvas.save()
        canvas.rotate(hourAngle + 90F, centerX, centerY)
        println(hourAngle)
        // 计算时针长度并绘制
        val hourHandLength = radius * 0.5f
        val hourHandStartX = centerX + (hourHandLength * 0.3f)
        val hourHandEndX = centerX - (hourHandLength * 0.9f)
        canvas.drawLine(hourHandStartX, centerY, hourHandEndX, centerY, hourHandPaint)
        canvas.restore()
        // 绘制分针
        canvas.save()
        canvas.rotate(minuteAngle + 90F, centerX, centerY)
        // 计算分针长度并绘制
        val minuteHandLength = radius * 0.7f
        val minuteHandStartX = centerX + (minuteHandLength * 0.1f)
        val minuteHandEndX = centerX - (minuteHandLength * 0.9f)
        canvas.drawLine(minuteHandStartX, centerY, minuteHandEndX, centerY, minuteHandPaint)
        canvas.restore()
        // 绘制秒针
        canvas.save()
        canvas.rotate(secondAngle + 90F, centerX, centerY)
        // 计算秒针长度并绘制
        val secondHandLength = radius * 0.99f
        val secondHandStartX = centerX + (secondHandLength * 0.1f)
        val secondHandEndX = centerX - (secondHandLength * 0.9f)
        canvas.drawLine(secondHandStartX, centerY, secondHandEndX, centerY, secondHandPaint)
        canvas.restore()
    }

    private fun drawHourNumbers(canvas: Canvas) {
        val hourRadius = radius * 0.8f
        val degreesPerHour = 360f / 12
        for (hour in 1..12) {
            val angle = Math.toRadians((degreesPerHour * hour - 90).toDouble())
            val numberX = centerX + hourRadius * Math.cos(angle).toFloat()
            val numberY = centerY + hourRadius * Math.sin(angle).toFloat()
            canvas.drawText(hour.toString(), numberX, numberY, numberPaint)
        }
    }


    private fun drawMinuteLines(canvas: Canvas) {
        val degreesPerMinute = 360f / 60
        for (minute in 1..60) {
            val angle = Math.toRadians((degreesPerMinute * minute - 90).toDouble())
            val startX: Float
            val startY: Float
            val endX: Float
            val endY: Float
            if (minute % 5 == 0) { // 整点刻度加粗
                linePaint.strokeWidth = 6f
                startX = centerX + radius * 0.93f * Math.cos(angle).toFloat()
                startY = centerY + radius * 0.93f * Math.sin(angle).toFloat()
                endX = centerX + radius * Math.cos(angle).toFloat()
                endY = centerY + radius * Math.sin(angle).toFloat()
            } else {
                linePaint.strokeWidth = 5f
                startX = centerX + radius * 0.95f * Math.cos(angle).toFloat()
                startY = centerY + radius * 0.95f * Math.sin(angle).toFloat()
                endX = centerX + radius * Math.cos(angle).toFloat()
                endY = centerY + radius * Math.sin(angle).toFloat()
            }
            canvas.drawLine(startX, startY, endX, endY, linePaint)
        }
    }
}
